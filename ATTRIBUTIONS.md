All of the sound effects are taken from https://freesound.org



Attributions
============

All samples have been manipulated by me. (Such as shortening, mixing down to mono, reducing the bit rate etc).
Please use the links to find the originals.
If you re-use any of these samples, make sure you credit the original creators.

For each sample, I give :

* Its original name
* Name of the creator
* A url of the original file
* The role of the sound sample
* The name of the file as used in the game


shipExplosion
-------------
33245__ljudman__grenade.wav
ljudman
http://www.freesound.org/samplesViewSingle.php?id=33245
Ship explosion
Removed the short silence at the beginning

sendingCargo
------------
27568__suonho__MemoryMoon_space_blaster_plays.wav
suonho
http://www.freesound.org/samplesViewSingle.php?id=27568
Cargo enters a Gate

fire
----
45656__csengeri__tuzijatek.wav
csengeri
http://www.freesound.org/samplesViewSingle.php?id=45656
Used the 2nd noise, trimmed, and faded out the end to make it very short.
Ship firing a bullet

thrust
------
22456__nathanshadow__Thruster_Level_III.aif
nathanshadow
http://www.freesound.org/samplesViewSingle.php?id=22456
When the ship's thrusters are fired

rotate
------
22456__nathanshadow__Thruster_Level_III.aif
nathanshadow
http://www.freesound.org/samplesViewSingle.php?id=22456
When the ship turns left or right.
Changed the pitch and decreased the volume a lot.
rotate.ogg

ricochet
--------
74395__Benboncan__Ricochet_3.wav
Benboncan
http://www.freesound.org/samplesViewSingle.php?id=74395
Bullets hitting scenery

drip
----
12654__mich3d__Drop_Slow_Long_01.wav
www.bonson.ca
http://www.freesound.org/samplesViewSingle.php?id=12654
Cut just a single drip from the middle

missile
-------
18380__inferno__hvrl.wav
inferno
http://www.freesound.org/samplesViewSingle.php?id=18380
Lauch of a missile

ballCollected
-------------
31652__ERH__kadel_3.wav
ERH
http://www.freesound.org/samplesViewSingle.php?id=31652
When the ball has travelled through the gate.

door
----
Sliding Noise V2.wav
cbenci
https://freesound.org/people/cbenci/sounds/325585/
Door sliding open/closed

gateOpen
--------
29594__ERH__choir_c.wav
ERH
http://www.freesound.org/samplesViewSingle.php?id=29594
Chopped the end to make a little shorter
Gate open for a ship to move through.

ding
----
39320__the-bizniss__glass-d.wav
THE_bizniss
http://www.freesound.org/people/THE_bizniss/sounds/39320/
A piece of cargo has been picked up. Also when WaterCargo is filled.

brokenGlass
-----------
Breaking glass.mp3
the_epic1357
https://freesound.org/people/the_epic1357/sounds/591069/
A WaterCargo breaks

glug
----
drain finishing/glug
brittmosel
https://freesound.org/people/brittmosel/sounds/529300/
Water Cago begin filled

goldExplosion
-------------
20070915.3explosions.wav
40969__dobroide__20
070915.3explosions.wav
http://www.freesound.org/samplesViewSingle.php?id=40969
Gold Cargo explodes

=============
Plan to use :
=============

19931__FreqMan__pulsing_increase.wav
http://www.freesound.org/samplesViewSingle.php?id=19931
Countdown to a building / object exploding (as in thrust).

http://www.freesound.org/samplesViewSingle.php?id=12654
12654__mich3d__Drop_Slow_Long_01.wav

